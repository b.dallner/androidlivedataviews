package de.dbprojects.daybydaytodolist_v2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;

public class NewTodoListActivity extends AppCompatActivity
{
    public static final String EXTRA_REPLY = "de.dbprojects.daybydaytodolist_v2.todolistssql.REPLY";

    private EditText editNewTodoList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_todo_list);

        editNewTodoList = findViewById(R.id.edit_new_todo_list);

        final Button buttonSaveNewTodoList = findViewById(R.id.button_save_new_todo_list);
        buttonSaveNewTodoList.setOnClickListener(v -> {
            Intent replyIntent = new Intent();
            if(TextUtils.isEmpty(editNewTodoList.getText()))
            {
                setResult(RESULT_CANCELED, replyIntent);
            }
            else
            {
                String todoListName = editNewTodoList.getText().toString();
                replyIntent.putExtra(EXTRA_REPLY, todoListName);
                setResult(RESULT_OK, replyIntent);
            }

            finish();
        });
    }
}