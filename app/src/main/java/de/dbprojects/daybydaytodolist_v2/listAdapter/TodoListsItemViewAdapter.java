package de.dbprojects.daybydaytodolist_v2.listAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import de.dbprojects.daybydaytodolist_v2.MainActivity;
import de.dbprojects.daybydaytodolist_v2.R;
import de.dbprojects.daybydaytodolist_v2.entity.TodoListEntity;
import de.dbprojects.daybydaytodolist_v2.interfaces.OnClickListener;

public class TodoListsItemViewAdapter extends RecyclerView.Adapter<TodoListsItemViewAdapter.ViewHolder>
{
    private final LayoutInflater  inflater;
    private       OnClickListener onClickListener;

    private List<TodoListEntity> items;// Cached copy of todoLists

    public TodoListsItemViewAdapter(Context context, OnClickListener onClickListener)
    {
        inflater             = LayoutInflater.from(context);
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = inflater.inflate(R.layout.todo_lists_item, parent, false);
        return new ViewHolder(view, onClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position)
    {
        if (items != null)
        {
            holder.mItem = items.get(position);
            holder.mIdView.setText(String.valueOf(items.get(position).todoListID));
            holder.mContentView.setText(items.get(position).getTodoListName());
        }
        else
        {
            holder.mContentView.setText(R.string.no_todo_lists_found);
        }
    }

    @Override
    public int getItemCount()
    {
        if (items != null)
        {
            return items.size();
        }

        return 0;
    }

    public void setItems(List<TodoListEntity> items)
    {
        this.items = items;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        private static final String TAG = "TodoListViewHolder";

        public final View            mView;
        public final TextView        mIdView;
        public final TextView        mContentView;
        public       TodoListEntity  mItem;
        private      OnClickListener mOnClickListener;

        public ViewHolder(View view, OnClickListener onClickListener)
        {
            super(view);
            mView            = view;
            mIdView          = view.findViewById(R.id.item_number);
            mContentView     = view.findViewById(R.id.content);
            mOnClickListener = onClickListener;

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view)
        {
            mOnClickListener.onClickItem(getAdapterPosition());
        }

        @NonNull
        @Override
        public String toString()
        {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}