package de.dbprojects.daybydaytodolist_v2.listAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import de.dbprojects.daybydaytodolist_v2.R;
import de.dbprojects.daybydaytodolist_v2.entity.TodoEntity;

public class TodoListItemViewAdapter extends RecyclerView.Adapter<TodoListItemViewAdapter.ViewHolder>
{
    private final LayoutInflater   inflater;
    private       List<TodoEntity> items;// Cached copy of todos

    public TodoListItemViewAdapter(Context context)
    {
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = inflater.inflate(R.layout.todo_list_item, parent, false);
        return new TodoListItemViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        if (items != null)
        {
            holder.mItem = items.get(position);
            holder.mIdView.setText(String.valueOf(items.get(position).todoID));
            holder.mContentView.setText(items.get(position).getTodoName());
        }
        else
        {
            holder.mContentView.setText(R.string.no_todos_found);
        }
    }

    @Override
    public int getItemCount()
    {
        if (items != null)
        {
            return items.size();
        }

        return 0;
    }

    public void setItems(List<TodoEntity> items)
    {
        this.items = items;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public final View       mView;
        public final TextView   mIdView;
        public final TextView   mContentView;
        public       TodoEntity mItem;

        public ViewHolder(@NonNull View view)
        {
            super(view);

            mView        = view;
            mIdView      = view.findViewById(R.id.item_number);
            mContentView = view.findViewById(R.id.content);
        }

        @Override
        public String toString()
        {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
