package de.dbprojects.daybydaytodolist_v2.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "Todos", foreignKeys = @ForeignKey(entity = TodoListEntity.class, parentColumns = "todoListID", childColumns = "fkTodoListID", onDelete = ForeignKey.CASCADE))
public class TodoEntity
{
    @PrimaryKey(autoGenerate = true)
    public int todoID;

    @NonNull
    @ColumnInfo
    private int fkTodoListID;

    @ColumnInfo
    private String todoName;

    public TodoEntity(@NonNull int fkTodoListID, String todoName)
    {
        this.todoName     = todoName;
        this.fkTodoListID = fkTodoListID;
    }

    @NonNull
    public int getFkTodoListID()
    {
        return fkTodoListID;
    }

    public void setFkTodoListID(@NonNull int fkTodoListID)
    {
        this.fkTodoListID = fkTodoListID;
    }

    public String getTodoName()
    {
        return todoName;
    }

    public void setTodoName(String todoName)
    {
        this.todoName = todoName;
    }
}
