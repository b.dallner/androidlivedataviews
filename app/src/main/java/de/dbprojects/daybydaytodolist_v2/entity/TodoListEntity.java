package de.dbprojects.daybydaytodolist_v2.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "TodoLists")
public class TodoListEntity implements Parcelable
{
    public static final Creator<TodoListEntity> CREATOR = new Creator<TodoListEntity>()
    {
        @Override
        public TodoListEntity createFromParcel(Parcel in)
        {
            return new TodoListEntity(in);
        }

        @Override
        public TodoListEntity[] newArray(int size)
        {
            return new TodoListEntity[size];
        }
    };

    @PrimaryKey(autoGenerate = true)
    public int todoListID;

    @ColumnInfo
    private String todoListName;

    public TodoListEntity(String todoListName)
    {
        //Parameter müssen den selben Namen wie Klassenvaribalen haben
        this.todoListName = todoListName;
    }

    protected TodoListEntity(Parcel in)
    {
        todoListID   = in.readInt();
        todoListName = in.readString();
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(todoListID);
        dest.writeString(todoListName);
    }

    @NonNull
    public String getTodoListName()
    {
        return todoListName;
    }
}
