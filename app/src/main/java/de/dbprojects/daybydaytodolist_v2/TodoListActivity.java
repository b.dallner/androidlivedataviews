package de.dbprojects.daybydaytodolist_v2;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import de.dbprojects.daybydaytodolist_v2.entity.TodoListEntity;
import de.dbprojects.daybydaytodolist_v2.factory.TodoViewModelFactory;
import de.dbprojects.daybydaytodolist_v2.listAdapter.TodoListItemViewAdapter;
import de.dbprojects.daybydaytodolist_v2.viewModel.TodoViewModel;

public class TodoListActivity extends AppCompatActivity
{
    public static final  String        SELECTED_TODO_LIST = "SelectedTodoList";
    private static final String        TAG                = "TodoListActivity";
    private              TodoViewModel todoViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_list);

        if (getIntent().hasExtra(SELECTED_TODO_LIST))
        {
            TodoListEntity entity = getIntent().getParcelableExtra(SELECTED_TODO_LIST);

            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle(entity.getTodoListName());

            RecyclerView                  recyclerView = findViewById(R.id.view_todo_list);
            final TodoListItemViewAdapter adapter      = new TodoListItemViewAdapter(this);

            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

            todoViewModel = new ViewModelProvider(this, new TodoViewModelFactory(this.getApplication(), entity.todoListID)).get(TodoViewModel.class);
            todoViewModel.getTodos().observe(this, adapter::setItems);
        }
    }
}
