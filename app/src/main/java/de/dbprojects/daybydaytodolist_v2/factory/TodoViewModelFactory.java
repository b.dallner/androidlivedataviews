package de.dbprojects.daybydaytodolist_v2.factory;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import de.dbprojects.daybydaytodolist_v2.viewModel.TodoViewModel;

public class TodoViewModelFactory implements ViewModelProvider.Factory
{
    private Application mApplication;
    private int         mTodoListID;

    public TodoViewModelFactory(Application mApplication, int mTodoListID)
    {
        this.mApplication = mApplication;
        this.mTodoListID  = mTodoListID;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass)
    {
        return (T) new TodoViewModel(mApplication, mTodoListID);
    }
}
