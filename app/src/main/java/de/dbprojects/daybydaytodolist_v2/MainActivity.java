package de.dbprojects.daybydaytodolist_v2;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import de.dbprojects.daybydaytodolist_v2.entity.TodoListEntity;
import de.dbprojects.daybydaytodolist_v2.interfaces.OnClickListener;
import de.dbprojects.daybydaytodolist_v2.listAdapter.TodoListsItemViewAdapter;
import de.dbprojects.daybydaytodolist_v2.viewModel.TodoListViewModel;

public class MainActivity extends AppCompatActivity implements OnClickListener
{
    private static final String TAG = "MainActivity";

    private static final int NEW_TODO_LIST_ACTIVITY_REQUEST_CODE = 1;

    private TodoListViewModel todoListViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        RecyclerView                   todoLists = findViewById(R.id.view_todo_lists);
        final TodoListsItemViewAdapter adapter   = new TodoListsItemViewAdapter(this, this);

        todoLists.setAdapter(adapter);
        todoLists.setLayoutManager(new LinearLayoutManager(this));

        todoListViewModel = new ViewModelProvider(this).get(TodoListViewModel.class);
        todoListViewModel.getTodoLists().observe(this, adapter::setItems);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(MainActivity.this, NewTodoListActivity.class);
                startActivityForResult(intent, NEW_TODO_LIST_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_TODO_LIST_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK)
        {
            TodoListEntity listEntity = new TodoListEntity(data.getStringExtra(NewTodoListActivity.EXTRA_REPLY));

            todoListViewModel.insert(listEntity);
        }
        else
        {
            Toast.makeText(getApplicationContext(), R.string.todo_list_name_empty, Toast.LENGTH_LONG)
                    .show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClickItem(int position)
    {
        Intent intent = new Intent(MainActivity.this, TodoListActivity.class);

        intent.putExtra(TodoListActivity.SELECTED_TODO_LIST, todoListViewModel.getTodoLists().getValue().get(position));

        startActivity(intent);
    }
}