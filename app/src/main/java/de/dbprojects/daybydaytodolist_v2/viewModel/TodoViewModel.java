package de.dbprojects.daybydaytodolist_v2.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import de.dbprojects.daybydaytodolist_v2.data.TodoRepository;
import de.dbprojects.daybydaytodolist_v2.entity.TodoEntity;

public class TodoViewModel extends AndroidViewModel
{
    private TodoRepository             repository;
    private LiveData<List<TodoEntity>> todos;

    public TodoViewModel(@NonNull Application application, int todoListID)
    {
        super(application);
        repository = new TodoRepository(application);

        todos = repository.getTodosByTodoListID(todoListID);
    }

    public LiveData<List<TodoEntity>> getTodos()
    {
        return todos;
    }
}
