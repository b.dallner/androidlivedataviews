package de.dbprojects.daybydaytodolist_v2.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import de.dbprojects.daybydaytodolist_v2.data.TodoRepository;
import de.dbprojects.daybydaytodolist_v2.entity.TodoListEntity;

public class TodoListViewModel extends AndroidViewModel
{
    private TodoRepository                 repository;
    private LiveData<List<TodoListEntity>> todoLists;

    public TodoListViewModel(@NonNull Application application)
    {
        super(application);
        repository = new TodoRepository(application);
        todoLists  = repository.getTodoLists();
    }

    public void insert(TodoListEntity entity)
    {
        repository.insert(entity);
    }

    public LiveData<List<TodoListEntity>> getTodoLists()
    {
        return todoLists;
    }
}
