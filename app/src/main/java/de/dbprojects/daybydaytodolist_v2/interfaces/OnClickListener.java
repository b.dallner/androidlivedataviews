package de.dbprojects.daybydaytodolist_v2.interfaces;

public interface OnClickListener
{
    void onClickItem(int position);
}
