package de.dbprojects.daybydaytodolist_v2.data;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.dbprojects.daybydaytodolist_v2.dao.TodoDao;
import de.dbprojects.daybydaytodolist_v2.dao.TodoListDao;
import de.dbprojects.daybydaytodolist_v2.entity.TodoEntity;
import de.dbprojects.daybydaytodolist_v2.entity.TodoListEntity;

@Database(entities = {TodoListEntity.class, TodoEntity.class}, version = 2, exportSchema = false)
public abstract class TodoDatabase extends RoomDatabase
{
    static final Migration MIGRATION_1_2 = new Migration(1, 2)
    {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database)
        {
            database.execSQL("CREATE TABLE IF NOT EXISTS `Todos` (" +
                        "`todoID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                        "`fkTodoListID` INTEGER NOT NULL, " +
                        "`todoName` TEXT, "+
                        "FOREIGN KEY('fkTodoListID') REFERENCES `TodoLists`(`todoListID`) ON UPDATE NO ACTION ON DELETE CASCADE" +
                    ")");
        }
    };

    private static final int             NUMBER_OF_THREADS = 4;
    public static final  ExecutorService writeExecutor     = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    private static volatile TodoDatabase INSTANCE;

    private static RoomDatabase.Callback callback = new RoomDatabase.Callback()
    {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db)
        {
            //Beim erstellen der Datenbank die TodoListe "Default"
            super.onCreate(db);

            writeExecutor.execute(() -> {
                TodoListDao dao = INSTANCE.todoListDao();

                TodoListEntity entity = new TodoListEntity("Default");

                dao.insert(entity);
            });

            writeExecutor.execute(() -> {
                TodoDao dao = INSTANCE.todoDao();

                TodoEntity entity = new TodoEntity(1, "first todo");

                dao.insert(entity);
            });
        }
    };

    public static TodoDatabase getInstance(final Context context)
    {
        if (INSTANCE == null)
        {
            synchronized (TodoDatabase.class)
            {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(), TodoDatabase.class, "DayByDayTodoList")
                        .addCallback(callback)
                        .addMigrations(MIGRATION_1_2)
                        .build();
            }
        }

        return INSTANCE;
    }

    public abstract TodoDao todoDao();

    public abstract TodoListDao todoListDao();
}
