package de.dbprojects.daybydaytodolist_v2.data;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

import de.dbprojects.daybydaytodolist_v2.dao.TodoDao;
import de.dbprojects.daybydaytodolist_v2.dao.TodoListDao;
import de.dbprojects.daybydaytodolist_v2.entity.TodoEntity;
import de.dbprojects.daybydaytodolist_v2.entity.TodoListEntity;

public class TodoRepository
{
    private TodoListDao todoListDao;
    private TodoDao     todoDao;

    private LiveData<List<TodoListEntity>> todoLists;
    private LiveData<List<TodoEntity>>     todos;

    public TodoRepository(Application application)
    {
        TodoDatabase db = TodoDatabase.getInstance(application);

        todoListDao = db.todoListDao();
        todoDao     = db.todoDao();

        todoLists   = db.todoListDao().getTodoLists();
    }

    public LiveData<List<TodoListEntity>> getTodoLists()
    {
        return todoLists;
    }

    public LiveData<List<TodoEntity>> getTodosByTodoListID(int todoListID)
    {
        todos = todoDao.getTodosByTodoListID(todoListID);
        return todos;
    }

    public void insert(TodoListEntity entity)
    {
        //Der writeExecutor wird benötigt damit der insert query auf einem eigenen thread
        //ausgeführt wird und nicht den UI thread blockiert.
        //Lambda expression die die Funktion insert der TodoListDao Klassen ausführt.
        TodoDatabase.writeExecutor.execute(() -> todoListDao.insert(entity));
    }
}
