package de.dbprojects.daybydaytodolist_v2.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import de.dbprojects.daybydaytodolist_v2.entity.TodoListEntity;

@Dao
public interface TodoListDao
{
    @Delete
    void delete(TodoListEntity listEntity);

    @Query("SELECT * FROM TodoLists")
    LiveData<List<TodoListEntity>> getTodoLists();

    //Listen mit dem selben Namen erlauben
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(TodoListEntity todoList);
}
