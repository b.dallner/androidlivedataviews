package de.dbprojects.daybydaytodolist_v2.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import de.dbprojects.daybydaytodolist_v2.entity.TodoEntity;

@Dao
public interface TodoDao
{
    @Delete
    void delete(TodoEntity todoEntity);

    @Query("SELECT * FROM Todos")
    LiveData<List<TodoEntity>> getTodos();

    @Query("Select * FROM Todos WHERE fkTodoListID = :id")
    LiveData<List<TodoEntity>> getTodosByTodoListID(int id);

    //Listen mit dem selben Namen erlauben
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(TodoEntity todoEntity);
}
